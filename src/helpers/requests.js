import axios from 'axios'
import { URL } from '../utils/constants'

export const getNewsInfo = async (type, content) => {
    try {
        const getNewsInfoResponse = await axios.get(`${URL.API_URL}/News/webmethods/verify`, {
            params: {
                type,
                content
            }
        })
        if (!!getNewsInfoResponse.data.id) {
            return getNewsInfoResponse.data.id
        }
    } catch (e) {
        console.log('getNewsInfoError', e)
        return await postNews(type, content)
    }
}

export const postNews = async (type, content) => {
    const postNewsResponse = await axios.post(`${URL.API_URL}/News`, {
        type,
        content
    })
    if (!!postNewsResponse.data.id) {
        return postNewsResponse.data.id
    }
}