import { getNewsInfo } from '../helpers/requests'
import { URL, CONTENT_TYPE } from '../utils/constants'

export const selectedTextHandler = async (info) => {
    const newsId = await getNewsInfo(CONTENT_TYPE.TEXT, info.selectionText)
    chrome.tabs.create({ url: `${URL.PAGE_URL}/verify/${newsId}`})
}

export const selectedLinkHandler = async (info) => {
    const newsId = await getNewsInfo(CONTENT_TYPE.LINK, info.pageUrl)
    chrome.tabs.create({ url: `${URL.PAGE_URL}/verify/${newsId}`})
}