export const URL = {
    API_URL: 'https://api.czytoprawda.pl',
    PAGE_URL: 'https://www.czytoprawda.pl'
}

export const CONTENT_TYPE = {
    TEXT: 'text',
    LINK: 'link'
}