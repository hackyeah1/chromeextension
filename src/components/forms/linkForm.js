import React from 'react';
import { useFormik } from 'formik';
import * as Yup from "yup"
import { formSubmit } from "../../helpers"

const LinkFormRender = () => {
    const formik = useFormik({
        initialValues: {
            linkValue: ''
        },
        validationSchema: Yup.object({
            linkValue: Yup.string().required('Pole Wymagane')
        }),
        onSubmit: values => {
            formSubmit(values)
        }
    })
    // console.log('formikformik', formik.isSubmitting) //TODO
    return <form onSubmit={formik.handleSubmit}>
        <label>Wpisz link do sprawdzenia</label>
        <input
            name={'linkValue'}
            {...formik.getFieldProps('linkValue')}
        />
        {formik.touched.linkValue && formik.errors.linkValue ? (
            <div>{formik.errors.linkValue}</div>
        ) : null}
        <button type={'submit'}>Wyślij</button>
    </form>
}

export const LinkForm = LinkFormRender