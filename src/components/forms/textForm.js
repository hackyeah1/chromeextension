import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { formSubmit } from "../../helpers"

const TextFormRender = () => {
    const formik = useFormik({
        initialValues: {
            textValue: ''
        },
        validationSchema: Yup.object({
                textValue: Yup.string().required('Pole Wymagane')
        }),
        onSubmit: values => {
            formSubmit(values)
        }
    })
    // console.log('formikformik', formik.isSubmitting) //TODO
    return <form onSubmit={formik.handleSubmit}>
        <label>Wpisz tekst do sprawdzenia</label>
        <input
            name={'textValue'}
            {...formik.getFieldProps('textValue')}
        />
        {formik.touched.textValue && formik.errors.textValue ? (
            <div>{formik.errors.textValue}</div>
        ) : null}
        <button type={'submit'}>Wyślij</button>
    </form>
}

export const TextForm = TextFormRender