import { selectedTextHandler, selectedLinkHandler } from "../../helpers/contextMenuHandlers"
import '../../assets/img/icon-34.png';
import '../../assets/img/icon-128.png';
import '../../assets/img/dark/34.png';
import '../../assets/img/dark/128.png';

const setUpContextMenus = () => {
    chrome.contextMenus.create({
        title: 'Czy to prawda?',
        id: 'forSelectionId',
        contexts: ['selection'],
        onclick: selectedTextHandler
    })

    chrome.contextMenus.create({
        title: 'Czy to prawda?',
        id: 'forLinkId',
        contexts: ['link'],
        onclick: selectedLinkHandler
    })
}

chrome.runtime.onInstalled.addListener(() => {
    setUpContextMenus();
});