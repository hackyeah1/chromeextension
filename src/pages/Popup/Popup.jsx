import React, { useState } from 'react';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import dark64 from '../../assets/img/dark/64.png'
import './Popup.css';
import { selectedLinkHandler, selectedTextHandler } from "../../helpers/contextMenuHandlers"
import { validURL } from "../../helpers"

const onClickHandler = (inputValue) => {
    validURL(inputValue) ? selectedLinkHandler({ pageUrl: inputValue }) : selectedTextHandler({ selectionText: inputValue })
}

const Popup = () => {
  const [inputValue, setInputValue] = useState('')
  return (
      <div className="App">
          <div className="p-grid p-dir-col grid">
              <div className="p-col">
                  <img src={dark64} alt="logo" />
              </div>
              <div className="p-col">
                  <h1>Czy to prawda?</h1>
                  <p className={'description'}>Wyślij fragment tekstu lub linku, który wydaje Ci się podejrzany. Powiemy Ci, czy to fake news lub przekażemy moderatorom do weryfikacji.</p>
              </div>
              <div className="p-col">
                  <InputText
                      className={'textInput'}
                      value={inputValue}
                      onChange={(e) => setInputValue(e.target.value)}
                      placeholder={'np. Zwierzęta przenoszą koronawirusa'} />
              </div>
              <div className="p-col">
                  <Button label={'SPRAWDŹ TEN NEWS'} className={'button'} disabled={!inputValue} onClick={() => onClickHandler(inputValue)} />
              </div>
          </div>
      </div>
  );
};

export default Popup;
