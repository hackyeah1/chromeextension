import React from 'react';
import { render } from 'react-dom';

import Popup from './Popup';
import './index.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import "primeflex/primeflex.css";

render(<Popup />, window.document.querySelector('#app-container'));
